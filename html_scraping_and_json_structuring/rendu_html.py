import json

input_data = ''
with open('data_elements_struct.json') as f:
    input_data = json.load(f)

output_html = """
<!DOCTYPE html>
<html>

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="referrer" content="no-referrer">

    <meta name="description" content="Restructured results">
<title>
    Restructured results
</title>

<link rel="stylesheet" href="nice.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Didact+Gothic">

</head>

<body>

<nav role="navigation">
"""

# Opening the products list
# output_html += '<ul>'

for _product in input_data:
    # product name + link
    output_html += '<h1>Product: <a href="' + _product['url'] + '">' + _product['name'] + '</a></h1>\n'
    # output_html += '  <li>Product: <a href="' + _product['url'] + '">' + _product['name'] + '</a></li>\n'

    # Opening the guides list
    # output_html += '  <ul>\n'

    # product guides
    for _guide in _product['guides']:
        output_html += '<h3>&gt; Guide: ' + _guide['name'] + '</h3>\n'
        # output_html += '    <li>Guide: ' + _guide['name'] + '</li>\n'

        # Opening the pages list
        output_html += '    <ul>\n'

        # guide pages
        for _page in _guide['pages']:
            output_html += '      <li>Page: <a href="' + _page['url'] + '">' + _page['name'] + '</a></li>\n'
        
        # closing the pages list
        output_html += '    </ul>\n'

        # adding space
        output_html += '<hr />'

    # closing the guides list
    # output_html += '  </ul>\n'

# closing the products list
# output_html += '</ul>\n'

output_html += """
</nav>
</body>

</html>
"""

with open('rendu.html','w') as f:
    f.write(output_html)
