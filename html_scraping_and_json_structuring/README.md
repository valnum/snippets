The code of this folder performs the following steps:

1. HTML scraping, loading the `source.html` file and extracting relevant data into the `data_elements.json` JSON file --> done via `data_scraping.py`
1. restructuring, loading the `data_elements.json` file and restructuring it in a hierarchical structure into the `data_elements_struct.json` JSON file --> done via `results_structuring.py`
1. generating an HTML file, loading the `data_elements_struct.json` file and generating the `rendu.html` HTML file --> done via `rendu_html.py`

   note: the file renders with the `nice.css` file, stored in the `static` directory of this folder, and to be moved in the same folder as the HTML file's.

   this CSS file is taken from the [Call me Sam](https://github.com/victoriadrake/hugo-theme-sam/) Hugo theme by [Victoria Drake](https://victoria.dev/).
