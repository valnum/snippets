import json
from bs4 import BeautifulSoup

# functions to extract data elements
def is_div_with_class__search_result_text(tag):
    return tag.name == 'div' and tag.has_attr("class") and "search-result-text" in tag["class"]

def is_div_with_class__link_context(tag):
    return tag.name == 'div' and tag.has_attr("class") and "link-context" in tag["class"]

def is_span_with_class__search_result_field_value(tag):
    return tag.name == 'span' and tag.has_attr("class") and "search-result-field-value" in tag["class"]

# loading the file
html_data = ""
with open('source.html', encoding="utf-8") as _fobj:
    for _line in _fobj:
        html_data += _line

# BeautifulSoup-ing the HTML data
soup = BeautifulSoup(html_data, 'html.parser')

# scraping the data
results = list()

for _div in soup.find_all(is_div_with_class__search_result_text):

    _result_item = dict()
    _subdiv = _div.find(is_div_with_class__link_context)
    if _subdiv:
        _item_product = dict()
        _item_product['url'] = _subdiv.a["href"]
        _item_product['name'] = _subdiv.a.string
        _result_item['product'] = _item_product

    _subspan = _div.find(is_span_with_class__search_result_field_value)
    if _subspan:
        _item_guide = dict()
        _item_guide['name'] = _subspan.string
        _result_item['guide'] = _item_guide

    _item_page = dict()
    _item_page_url = _div.h2.a['href']
    _item_page_url = _item_page_url[:_div.h2.a['href'].rfind('?')]
    _item_page['url'] = _item_page_url
    _item_page['name'] = _div.h2.a.string
    _result_item['page'] = _item_page

    results.append(_result_item)

# generating the JSON file
with open('data_elements.json','w') as f:
    f.write(json.dumps(results))
