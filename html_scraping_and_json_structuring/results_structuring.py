import json

# Objectif: structurer selon product > guide > page

# lire les données en entrée
with open('data_elements.json') as f:
    input_data = json.load(f)

# initier la structure en sortie
output_data = list()

# pour chaque élément de la structure d'entrée
for _input_element in input_data:
    # trouver le product s'il est déjà répertorié
    # par défaut, on suppose que celui-ci n'est pas encore répertorié
    _output_product = dict()

    for _product in output_data:
        if _product['name'] == _input_element['product']['name']:
            _output_product = _product

    # si pas répertorié, on le répertorie
    if len(_output_product) == 0:
        output_data.append(_output_product)
        _output_product['name'] = _input_element['product']['name']
        _output_product['url'] = _input_element['product']['url']
        _output_product['guides'] = list()

    # pointer vers la liste des guides
    _output_guides = _output_product['guides']


    # Trouver le guide s'il est déjà répertorié
     # par défaut, on suppose que celui-ci n'est pas encore répertorié
    _output_guide = dict()

    for _guide in _output_product['guides']:
        if _guide['name'] == _input_element['guide']['name']:
            _output_guide = _guide

    # si pas répertorié, on le répertorie
    if len(_output_guide) == 0:
        _output_guides.append(_output_guide)
        _output_guide['name'] = _input_element['guide']['name']
        _output_guide['pages'] = list()

    # pointer vers la liste des pages
    _output_pages = _output_guide['pages']


    # répertorier la page
    _output_page = dict()
    _output_pages.append(_output_page)
    _output_page['name'] = _input_element['page']['name']
    _output_page['url'] = _input_element['page']['url']

# sauvegarder la structure en sortie
with open('data_elements_struct.json','w') as f:
    f.write(json.dumps(output_data))
